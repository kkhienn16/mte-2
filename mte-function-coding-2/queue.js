let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    //add code here
    collection[collection.length] = element;
    return collection;
    
}



function dequeue() {
    //add code here


    for (let i = 0; i < collection.length - 1; i++) {
            collection[i] = collection[i + 1];
        }

    collection.length--;

    return print();
    
}

function front() {
    //add code here
    return collection[0];
    
 
}

function size() {
    //add code here
   
    return collection.length;
}

function isEmpty() {
    //add code here
    if (collection.length > 0) {
        return false
    } else {
        return true
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
